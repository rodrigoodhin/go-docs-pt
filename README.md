<div align="center">

![](/assets/logo_small.png "go-docs-pt")

<br>
<img src="https://img.shields.io/badge/go-%2300ADD8.svg?style=for-the-badge&logo=go&logoColor=white">
<br><br>
<a href="#"><img src="https://img.shields.io/badge/test-success-green"></a>
<a href="#"><img src="https://img.shields.io/badge/build-passing-green"></a>
<a href="https://paypal.me/rodrigoodhin"><img src="https://img.shields.io/badge/donate-PayPal-blue"></a>
</div>

# GO-DOCS-PT

GO-DOCS-PT is a library written in pure Go providing the possibility to generate and validate Portuguese document number.
For now, only NIF, NISS, BI, NIB and IBAN are available.

&nbsp;
&nbsp;
&nbsp;

## Usage

Is it possible to import in a project

```go
import (
    "gitlab.com/rodrigoodhin/go-docs-pt/pkg/nif"
    "gitlab.com/rodrigoodhin/go-docs-pt/pkg/niss"
    "gitlab.com/rodrigoodhin/go-docs-pt/pkg/bi"
    "gitlab.com/rodrigoodhin/go-docs-pt/pkg/nib"
    "gitlab.com/rodrigoodhin/go-docs-pt/pkg/iban"
)

func main() {
	
    // Generate NIF, NISS, BI, NIB or IBAN number
    nif.Generate()
    niss.Generate()
    bi.Generate()
    nib.Generate()
    iban.Generate()

    // Validate NIF, NISS, BI, NIB or IBAN number
    nif.Validate("<NIF_NUMBER>")
    niss.Validate("<NISS_NUMBER>")
    bi.Validate("<BI_NUMBER>")
    nib.Validate("<NIB_NUMBER>")
    iban.Validate("<IBAN_NUMBER>")
}
```

or you can build and execute
```
go build -o goDocsPT
```

Usage of goDocsPT
```
Example:
./goDocsPT -t <DocumentType> -n <DocumentNumber> -v -g

-t = Document type - MANDATORY
-n = Document number - MANDATORY if tag -v is set
-v = Is to validate the document number
-g = Is to generate the document number

Available documents types:
NIF
NISS
BI
NIB
IBAN

```

Example:
```
./goDocsPT -t NIF -g 
./goDocsPT -t NIF -n 657370975 -v 
./goDocsPT -t NISS -g 
./goDocsPT -t NISS -n 12560858509 -v 
./goDocsPT -t BI -g 
./goDocsPT -t BI -n 912731328ZU1 -v 
./goDocsPT -t NIB -g 
./goDocsPT -t NIB -n 078160873252842073285 -v 
./goDocsPT -t IBAN -g 
./goDocsPT -t IBAN -n PT50078160873252842073285 -v 
```

&nbsp;
&nbsp;
&nbsp;

## LICENSE

[MIT License](/LICENSE)
