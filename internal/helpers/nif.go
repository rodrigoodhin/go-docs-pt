package helpers

import (
	"log"
	"math/rand"
	"strconv"
	"time"
)

const nifList = "1235689"
const nifMin = 1000000
const nifMax = 9999999

type NIF struct {
	Number string
}

func (n NIF) NumberOverList() int {
	rand.Seed(time.Now().UnixNano())

	num, err := strconv.Atoi(string(nifList[rand.Intn(len(nifList))]))
	if err != nil {
		log.Fatal(err)
	}

	return num
}

func (n NIF) NumberOverRange() int {
	rand.Seed(time.Now().UnixNano())

	return rand.Intn(nifMax-nifMin+1) + nifMin
}

func (n NIF) CheckDigit() (checkDigit int) {
	checkDigit = 0

	for i := 8; i > 0; i-- {
		num, _ := strconv.Atoi(string(n.Number[9-i-1]))
		checkDigit += (i + 1) * num
	}

	checkDigit = 11 - (checkDigit % 11)

	if checkDigit >= 10 {
		checkDigit = 0
	}

	return
}
