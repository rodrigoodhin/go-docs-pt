package helpers

import (
	"log"
	"math/rand"
	"strconv"
	"time"
)

var factors = []int{29, 23, 19, 17, 13, 11, 7, 5, 3, 2}
const nissMin = 1200000000
const nissMax = 1299999999

type NISS struct {
	Number string
}

func (n NISS) NumberOverRange() int {
	rand.Seed(time.Now().UnixNano())

	return rand.Intn(nissMax-nissMin+1) + nissMin
}

func (n NISS) CheckDigit() (checkDigit int) {
	sum := 0

	for i := 0; i < len(factors); i++{
		num, err := strconv.Atoi(string(n.Number[i]))
		if err != nil {
			log.Fatalln("Cannot convert NISS to int")
		}
		sum += num*factors[i]
	}

	checkDigit =  9 - ((sum) % 10)

	return
}
