package helpers

import (
	"math/rand"
	"time"
)

const biList = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
const biFullMin = 100000000
const biFullMax = 999999999
const biSmallMin = 0
const biSmallMax = 9

var alphabet = map[string]int{
	"0": 0, "1": 1, "2": 2, "3": 3, "4": 4, "5": 5,
	"6": 6, "7": 7, "8": 8, "9": 9, "A": 10, "B": 11,
	"C": 12, "D": 13, "E": 14, "F": 15, "G": 16, "H": 17,
	"I": 18, "J": 19, "K": 20, "L": 21, "M": 22, "N": 23,
	"O": 24, "P": 25, "Q": 26, "R": 27, "S": 28, "T": 29,
	"U": 30, "V": 31, "W": 32, "X": 33, "Y": 34, "Z": 35,
}

type BI struct {
	Number string
}

func (b BI) NumberOverList() (num string) {
	rand.Seed(time.Now().UnixNano())
	num = string(biList[rand.Intn(len(biList))])
	num += string(biList[rand.Intn(len(biList))])
	return
}

func (b BI)NumberOverFullRange() int {
	rand.Seed(time.Now().UnixNano())

	return rand.Intn(biFullMax-biFullMin+1) + biFullMin
}

func (b BI)NumberOverSmallRange() int {
	rand.Seed(time.Now().UnixNano())

	return rand.Intn(biSmallMax-biSmallMin+1) + biSmallMin
}

func (b BI) CheckDigit() (checkDigit int) {
	checkDigit = 0
	sd := false

	for i := 0; i < len(b.Number); i++ {
		num := alphabet[string(b.Number[len(b.Number)-1-i])]
		if sd {
			num = num*2
			if num > 9 {
				num = num - 9
			}
		}
		checkDigit += num
		sd = !sd
	}

	checkDigit = checkDigit % 10

	return
}
