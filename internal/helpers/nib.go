package helpers

import (
	"log"
	"math/rand"
	"strconv"
	"time"
)

var wiList = []int{ 73, 17, 89, 38, 62, 45, 53, 15, 50, 5, 49, 34, 81, 76, 27, 90, 9, 30, 3 }
const accountMin = 10000000000
const accountMax = 99999999999
const agencyMin = 1000
const agencyMax = 9999
var bankList = []string{"0007", "0010", "0018", "0019", "0023", "0025", "0032", "0033", "0034", "0035", "0036", "0038", "0043", "0045", "0046", "0059", "0061", "0065", "0079", "0193", "0235", "0269", "0500", "0781", "5180"}

type NIB struct {
	Number string
}

func (n NIB) NumberOverList() string {
	rand.Seed(time.Now().UnixNano())

	bank := n.GetBank()
	agency := strconv.Itoa(rand.Intn(agencyMax-agencyMin+1) + agencyMin)
	account := strconv.Itoa(rand.Intn(accountMax-accountMin+1) + accountMin)

	n.Number = bank+agency+account+"00"
	checkDigit := strconv.Itoa(n.CheckDigit())

	if len(checkDigit) == 1{
		checkDigit += "0"+checkDigit
	}

	return n.Number[0:19]+checkDigit
}

func (n NIB) GetBank() string {
	rand.Seed(time.Now().UnixNano())

	index := rand.Intn(25-0+1) + 0

	return bankList[index]
}

func (n NIB) CheckDigit() (checkDigit int) {
	checkDigit, err := strconv.Atoi(string(n.Number[19])+string(n.Number[20]))
	if err != nil {
		log.Fatal(err)
	}

	sum := 0
	for i := 0; i < 19; i++ {
		num, err := strconv.Atoi(string(n.Number[i]))
		if err != nil {
			log.Fatal(err)
		}

		sum += num * wiList[i]
	}

	checkDigit = 98 - (sum % 97)

	return
}