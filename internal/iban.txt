PT50
0007 - Novo Banco
0010 - BPI
0018 - Santander
0019 - BBVA
0023 - Activo Bank
0025 - Caixa BI
0032 - Barclays (é Bankinter com 0032 desde a mudança de marca)
0033 - Millennium BCP
0034 - BNP Paribas
0035 - CGD
0036 - Banco Montepio
0038 - Banif (passou para o Santander - 0018)
0043 - Abanca Portugal
0045 - Crédito Agrícola (Nacional)
0046 - Popular (passou para o Santander - 0018)
0059 - Caixa Económica da Misericórdia de Angra do Heroísmo
0061 - Big
0065 - Best
0079 - EuroBic
0193 - Banco CTT
0235 - Banco Carregosa
0269 - Bankinter (código anterior à aquisição do Barclays em Portugal)
0500 - ING Wholesale Banking
0781 - Direcção Geral do Tesouro
5180 - Caixa Central de Crédito Agrícola Mútuo (Lisboa)
