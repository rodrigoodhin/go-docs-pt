package main

import (
	"flag"
	"fmt"
	"gitlab.com/rodrigoodhin/go-docs-pt/pkg/bi"
	"gitlab.com/rodrigoodhin/go-docs-pt/pkg/iban"
	"gitlab.com/rodrigoodhin/go-docs-pt/pkg/nib"
	"gitlab.com/rodrigoodhin/go-docs-pt/pkg/nif"
	"gitlab.com/rodrigoodhin/go-docs-pt/pkg/niss"
	"os"
	"strconv"
	"strings"
)

func main() {

	docType := flag.String("t", "", "Document type")
	docNumber := flag.String("n", "", "Document number")
	toValidate := flag.Bool("v", false, "Is to validate the document number")
	toGenerate := flag.Bool("g", false, "Is to generate the document number")

	flag.Parse()

	if (*docType == "") ||
		(*toValidate && *docNumber == "") ||
		(!*toValidate && !*toGenerate) {
		usage()
		return
	}

	if *toValidate {
		isValid := false

		*docNumber = strings.ReplaceAll(*docNumber, " ", "")
		*docNumber = strings.ReplaceAll(*docNumber, "-", "")

		switch *docType {
		case "NIF": isValid = nif.Validate(*docNumber)
		case "NISS": isValid = niss.Validate(*docNumber)
		case "BI": isValid = bi.Validate(*docNumber)
		case "NIB": isValid = nib.Validate(*docNumber)
		case "IBAN": isValid = iban.Validate(*docNumber)
		}

		fmt.Println(*docType + " " + *docNumber + " is valid? " + strconv.FormatBool(isValid))
	}

	if *toGenerate {
		doc := ""

		switch *docType {
		case "NIF": doc = nif.Generate()
		case "NISS": doc = niss.Generate()
		case "BI": doc = bi.Generate()
		case "NIB": doc = nib.Generate()
		case "IBAN": doc = iban.Generate()
		}

		fmt.Println(*docType + " " + doc)
	}

}

func usage() {
	fmt.Printf("\nExample:\n%s -t <DocumentType> -n <DocumentNumber> -v -g\n\n", os.Args[0])
	fmt.Println("-t = Document type - MANDATORY")
	fmt.Println("-n = Document number - MANDATORY if tag -v is set")
	fmt.Println("-v = Is to validate the document number")
	fmt.Println("-g = Is to generate the document number")
	fmt.Printf("\n")
	fmt.Println("Available documents types:")
	fmt.Println("NIF")
	fmt.Println("NISS")
	fmt.Println("BI")
	fmt.Println("NIB")
	fmt.Printf("\n\n")
}
