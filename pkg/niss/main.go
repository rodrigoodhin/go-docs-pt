package niss

import (
	"fmt"
	"gitlab.com/rodrigoodhin/go-docs-pt/internal/helpers"
	"log"
	"strconv"
)

func Generate() (nif string) {
	n := helpers.NISS{}

	n.Number = strconv.Itoa(n.NumberOverRange())

	nif = fmt.Sprintf("%v%v", n.Number, n.CheckDigit())

	return
}

func Validate(niss string) (isValid bool) {
	n, err := strconv.Atoi(string(niss[len(niss)-1]))
	if err != nil {
		log.Fatalln("Cannot convert NISS to int")
	}

	checkDigit := helpers.NISS{Number: niss}.CheckDigit()

	if n == checkDigit && string(niss[0]) == "1" && string(niss[1]) == "2" && len(niss) == 11 {
		isValid = true
	}

	return
}
