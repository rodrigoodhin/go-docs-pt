package iban

import (
	"gitlab.com/rodrigoodhin/go-docs-pt/pkg/nib"
)

func Generate() (iban string) {
	return "PT50"+nib.Generate()
}

func Validate(iban string) (isValid bool) {
	return nib.Validate(iban[4:25])
}

