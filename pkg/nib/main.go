package nib

import (
	"gitlab.com/rodrigoodhin/go-docs-pt/internal/helpers"
	"log"
	"strconv"
)

func Generate() (nib string) {
	n := helpers.NIB{}

	nib = n.NumberOverList()

	return
}

func Validate(nib string) (isValid bool) {
	checkDigit := helpers.NIB{Number: nib}.CheckDigit()

	cd := checkDigit
	nv, err := strconv.Atoi(string(nib[19])+string(nib[20]))
	if err != nil {
		log.Fatal(err)
	}

	if cd == nv && len(nib) == 21 {
		isValid = true
	}

	return
}

