package bi

import (
	"fmt"
	"gitlab.com/rodrigoodhin/go-docs-pt/internal/helpers"
	"strconv"
)

func Generate() (bi string) {
	n := helpers.BI{}

	num := strconv.Itoa(n.NumberOverFullRange()) + `` + n.NumberOverList() + `` + strconv.Itoa(n.NumberOverSmallRange())

	if !Validate(num) {
		n.Number = Generate()
	} else {
		n.Number = num
	}

	bi = fmt.Sprintf("%v", n.Number)

	return
}

func Validate(bi string) (isValid bool) {
	checkDigit := helpers.BI{Number: bi}.CheckDigit()

	if checkDigit == 0 && len(bi) == 12 {
		isValid = true
	}

	return
}
