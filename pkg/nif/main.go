package nif

import (
	"fmt"
	"gitlab.com/rodrigoodhin/go-docs-pt/internal/helpers"
	"strconv"
)

func Generate() (nif string) {
	n := helpers.NIF{}

	n.Number = strconv.Itoa(n.NumberOverList()) + `` + strconv.Itoa(n.NumberOverRange())

	nif = fmt.Sprintf("%v%v", n.Number, n.CheckDigit())

	return
}

func Validate(nif string) (isValid bool) {
	checkDigit := helpers.NIF{Number: nif}.CheckDigit()

	if strconv.Itoa(checkDigit) == string(nif[8]) && len(nif) == 9 {
		isValid = true
	}

	return
}
